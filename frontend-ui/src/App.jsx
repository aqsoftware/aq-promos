import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect
} from 'react-router-dom';
import Main from './views/Main';
import Login from './views/Login';
import type { IAuthenticator } from './core/Interfaces';
import type { ParentRoute } from './core/Types';
import { Injector, Types } from './core';

const PrivateRoute = (props) => {
  let { authenticator, component, routes, ...rest } = props;
  return <Route {...rest} render={props => {
    if (authenticator.isAuthenticated()) {
      return React.createElement(component, {
        ...rest,
        ...props,
        user: authenticator.currentUser,
        appName: "Admin",
        appNameSmall: "Admin",
        routes: routes
      });
    }
    else {
      return (
        <Redirect to={{
          pathname: '/login'
        }} />
      );
    }
  }}
  />
}

type Props = {
  routes: Array<ParentRoute>
}

class App extends Component<Props> {

  authenticator: IAuthenticator;

  constructor(props: Props){
    super(props);
    this.authenticator = Injector.resolve(Types.authenticator);
  }

  render() {
    return (
      <Router>
        <div>
          <Route path='/login' component={Login}/>
          <PrivateRoute path='/' component={Main} authenticator={this.authenticator} routes={this.props.routes}/>
        </div>
      </Router>
    )
  }
}

export default App;
