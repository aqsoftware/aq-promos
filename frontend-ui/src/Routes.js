// @flow
import type { ParentRoute } from './core/Types';
import Promo from './pages/promo';

const routes: Array<ParentRoute> = [
  {
    name: 'Promo',
    sublinks: [
      {
        name: 'All Promos',
        path: '/promos',
        component: Promo
      }
    ]
  }
];

export default routes;