// @flow
import React from 'react';

export type Column = {
  name?: string,
  nameFn?: (any) => string,
  displayName?: string,
  headerStyle?: Object,
  bodyStyle?: Object,
  bodyClassName?: string
}

type Props = {
  title?: string,
  data: any,
  columns: Array<Column>,
  renderCell?: (cellInnerHtml: any, row: any, colIndex: number) => any
}

type State = {
}

export default class Table extends React.Component<Props, State> {

  static defaultProps = {
    data: [
      { id: "1234", name: "Test", description: "Test Description"},
      { id: "1234", name: "Test", description: "Test Description"},
      { id: "1234", name: "Test", description: "Test Description"}
    ],
    columns: [
      { name: "id", displayName: "#" },
      { name: "name" },
    ]
  };

  constructor(props: Props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const columns = this.props.columns.map((item, index) => {
      let style = {}
      if (typeof item.headerStyle !== 'undefined'){
        style = item.headerStyle;
      }

      let displayName = item.name;
      if (typeof item.displayName === 'string'){
        displayName = item.displayName;
      }
      return <th key={index} style={style}>{displayName}</th>
    });

    const rows = this.props.data.map((item, index) => {
      const cells = this.props.columns.map((col, colIndex) => {
        let cellInnerHtml = null;
        if (col.name){
          cellInnerHtml = item[col.name];
        }
        else if (col.nameFn) {
          cellInnerHtml = col.nameFn(item);
        }
        if (this.props.renderCell) {
          cellInnerHtml = this.props.renderCell(cellInnerHtml, item, colIndex);
        }
        let style = {}
        if (typeof col.bodyStyle !== 'undefined') {
          style = col.bodyStyle;
        }
        let className = ""
        if (typeof col.bodyClassName !== 'undefined') {
          className = col.bodyClassName;
        }
        return <td key={colIndex} style={style} className={className}>{cellInnerHtml}</td>;
      });
      return <tr key={index}>{cells}</tr>
    });

    let table = null;
    if (this.props.data) {

      let title = null;
      if (this.props.title) {
        title = <div className="box-header with-border">
          <h3 className="box-title">{this.props.title}</h3>
        </div>
      }

      table = <div className="box">
        {title}
        <div className="box-body table-responsive">
          <table className="table table-hover">
            <thead>
              <tr>
                {columns}
              </tr>
            </thead>
            <tbody>
              {rows}
            </tbody>
          </table>         
        </div>
      </div>
    }
    return table;
  }
}