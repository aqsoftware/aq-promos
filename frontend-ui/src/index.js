import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Injector from './core/Injector';
import Authenticator from './core/Authenticator';
import { Types } from './core/Types';
import routes from './Routes';

Injector.register(Types.authenticator, new Authenticator());

ReactDOM.render(
  <App routes={routes}/>,
  document.getElementById('root'));
registerServiceWorker();
