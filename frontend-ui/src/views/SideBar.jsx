// @flow
import React from 'react';
import { Link } from 'react-router-dom';
import type { User, ParentRoute } from '../core/Types';

type Props = {
  user: User,
  routes: Array<ParentRoute>
}

type State = {
}

export default class SideBar extends React.Component<Props, State> {

  static defaultProps = {
    user: {
      id: 'id',
      displayName: 'John Smith',
      avatarBig: 'http://lorempixel.com/160/160/people/1/',
      avatarSmall: 'http://lorempixel.com/160/160/people/1/'
    }
  };

  constructor(props: Props) {
    super(props);
    this.state = {
    };
  }

  render() {

    const links = this.props.routes.map((item, index) => {
      const subLinks = item.sublinks
      .filter((subItem) => {
        if (typeof subItem.showInSidebar === 'boolean') {
          return subItem.showInSidebar;
        }
        else {
          return true;
        }
      })
      .map((subItem, subIndex) => (
        <li key={index * 100 + subIndex}><Link to={subItem.path}>{subItem.name}</Link></li>
      ));
      return <li key={index} className="treeview active">
        <a href="#"><i className="fa fa-link"></i> <span>{item.name}</span>
          <span className="pull-right-container">
            <i className="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul className="treeview-menu">
          {subLinks}
        </ul>
      </li>
    });

    return (
      <section className="sidebar">
        <div className="user-panel">
          <div className="pull-left image">
            <img src={this.props.user.avatarSmall} className="img-circle" alt="User Image" />
          </div>
          <div className="pull-left info">
            <p>{this.props.user.displayName}</p>
            <a href="#"><i className="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <ul className="sidebar-menu" data-widget="tree">
          <li className="header">LINKS</li>          
          {links}
        </ul>
      </section>
    );
  }
}