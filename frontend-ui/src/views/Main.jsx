// @flow
import React from 'react';
import { Route } from 'react-router';
import Header from './Header';
import SideBar from './SideBar';
import Footer from './Footer';
import Home from './Home';
import type { User, ParentRoute } from '../core/Types';

type Props = {
  appName: [Object, string],
  appNameSmall: [Object, string],
  user: User,
  routes: Array<ParentRoute>
}

type State = {
  text: string
}

export default class Main extends React.Component<Props, State> {

  static defaultProps = {
    appName: <span><b>Admin</b>LTE</span>,
    appNameSmall: <span><b>A</b>LT</span>,
    user: {
      id: 'id',
      displayName: 'John Smith',
      avatarBig: 'http://lorempixel.com/160/160/people/1/',
      avatarSmall: 'http://lorempixel.com/160/160/people/1/'
    },
    routes: []
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      text: ""
    };
  }

  render() {
    const flatMap = (arr, fn) => (
      arr.map(fn).reduce((a, b) => { return a.concat(b); }, [])
    )

    const content = flatMap(this.props.routes, (item, index) => {
      return item.sublinks.map((subItem, subIndex) => {
        return React.createElement(subItem.component, {key: index, ...this.props});
      });
    });

    return (
      <div className="wrapper">
        <Header
          appName={this.props.appName}
          appNameSmall={this.props.appNameSmall}
          user={this.props.user}
        />
        <aside className="main-sidebar">
          <SideBar
            user={this.props.user}
            routes={this.props.routes}
          />
        </aside>
        {content}
        <Route exact path="/" component={Home} />        
        <Footer/>
      </div>
    );
  }
}
