// @flow
import React from 'react';

type Props = {
}

type State = {
}

export default class Home extends React.Component<Props, State> {

  static defaultProps = {};

  constructor(props: Props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>Home<small></small></h1>
        </section>
        <section className="content container-fluid">
          <div className="row">
            
          </div>
        </section>
      </div>   
    );
  }
}