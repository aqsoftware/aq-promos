// @flow
import React from 'react';
import { 
  Redirect 
} from 'react-router-dom';
import type { IAuthenticator } from '../core/Interfaces';
import { Injector, Types } from '../core';

type Props = {
}

type State = {
  redirectToReferrer: boolean
}

export default class MyClass extends React.Component<Props, State> {

  static defaultProps = {};
  authenticator: IAuthenticator;

  
  constructor(props: Props){
    super(props);
    this.authenticator = Injector.resolve(Types.authenticator);
    this.state = {
      redirectToReferrer: this.authenticator.isAuthenticated()
    };
  }

  authenticate() {    
    if (this.authenticator.isAuthenticated()) {
      this.setState({ redirectToReferrer: true});
    }
    else {
      this.authenticator.login(() => {
        this.setState({ redirectToReferrer: true });
      });
    }
  }

  render() {    
    if (this.state.redirectToReferrer) {
      return <Redirect to={{
        pathname: '/'
      }}/>
    }
    else {
      return (
        <div className="login-box">
          <div className="login-logo">
            <a href="../../index2.html"><b>Admin</b>LTE</a>
          </div>
          <div className="login-box-body">
            <p className="login-box-msg">Sign in to start your session</p>

            <div className="form-group has-feedback">
              <input type="email" className="form-control" placeholder="Email"/>
              <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div className="form-group has-feedback">
              <input type="password" className="form-control" placeholder="Password"/>
              <span className="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div className="row">
              <div className="col-xs-8">
                <div className="checkbox icheck">
                  <label>
                    <input type="checkbox"/> Remember Me
                  </label>
                </div>
              </div>
              <div className="col-xs-4">
                <button type="submit" className="btn btn-primary btn-block btn-flat" onClick={this.authenticate.bind(this)}>Sign In</button>
              </div>
            </div>

            <div className="social-auth-links text-center">
              <p>- OR -</p>
              <a href="#" className="btn btn-block btn-social btn-facebook btn-flat"><i className="fa fa-facebook"></i> Sign in using
                Facebook</a>
              <a href="#" className="btn btn-block btn-social btn-google btn-flat"><i className="fa fa-google-plus"></i> Sign in using
                Google+</a>
            </div>

            <a href="#">I forgot my password</a><br/>
            <a href="register.html" className="text-center">Register a new membership</a>
          </div>
        </div>
      );
    }
  }
}
