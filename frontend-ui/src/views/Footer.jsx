// @flow
import React from 'react';

type Props = {
  company: string
}

type State = {
}

export default class Footer extends React.Component<Props, State> {

  static defaultProps = {
    company: "Company"
  };

  constructor(props: Props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <footer className="main-footer">
        <div className="pull-right hidden-xs">
          <strong>v1.0.0</strong>
        </div>
        <strong>Copyright &copy; 2016 <a href="#">{this.props.company}</a>.</strong> All rights reserved.
      </footer>
    );
  }
}