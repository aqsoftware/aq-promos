// @flow

export type User = {
  id: string,
  displayName: string,
  avatarBig: string,
  avatarSmall: string
}

export type ChildRoute = {
  name: string,
  exact?: boolean,
  path: string,
  component: Object,
  showInSidebar?: boolean
}

export type ParentRoute = {
  name: string,
  sublinks: Array<ChildRoute>
}

export const Types = {
  authenticator: "authenticator"
}


export type Product = {
  id: string,
  createDate: string,
  lastModifiedDate: string,
  name: string,
  imageBig: ?string,
  imageSmall: ?string,
  productTypeId: number,
  creatorId: string,
  isActive: boolean,
  bmConversion: number,
  balance: number
}
export type Game = {
  id: string,
  createDate: string,
  lastModifiedDate: string,
  title: string,
  imageBig: string,
  imageSmall: string
}

export type PromoItem = {
  id: string,
  createDate: string,
  lastModifiedDate: string,
  category: number,
  promoTypeId: number,
  value: number,
  product: Product
}

export type Promo = {
  id: string,
  createDate: string,
  lastModifiedDate: string,
  game: Game,
  creatorId: string,
  items: Array<PromoItem>
}
