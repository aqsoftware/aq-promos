// @flow
import ApiClient from './ApiClient';

export default class GameApiClient extends ApiClient {

  getGames(start: number, limit: number): Promise<any> {
    const relativeUrl = `/games?start=${start}&limit=${limit}`;
    return this.request('GET', relativeUrl);
  }
}