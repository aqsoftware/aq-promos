// @flow
import 'whatwg-fetch'
import Hawk from 'hawk';

type FetchMethods = 'DELETE' | 'GET' | 'HEAD' | 'OPTIONS' | 'PATCH' | 'POST' | 'PUT';

export default class ApiClient {
  baseUrl: string;
  credentials: any;
  systemCredentials: any;

  constructor(baseUrl: string, credentials: any, systemCredentials: any) {
    this.baseUrl = baseUrl;
    this.credentials = credentials;
    this.systemCredentials = systemCredentials;
  }

  getAuthHeader(url: string, method: string, credentials: any): string {
    return Hawk.client.header(this.getFullUrl(url), method, { credentials: credentials, ext: 'aq-promos' }).field;
  }
  
  getFullUrl(url: string): string {
    return `${this.baseUrl}${url}`;
  }
  _request(method: FetchMethods, relativeUrl: string, credentials: any, body: ?any = null, headers: ?Object = {}, asJson: boolean = true): Promise<any> {
    console.info(`${method} ${this.getFullUrl(relativeUrl)}`);
    // console.info(`Auth = ${this.getAuthHeader(relativeUrl, method, credentials)}`);

    /*let actualBody: ?string = null;
    if (body) {
      actualBody = '' + JSON.stringify(body);
    }
    */
    let options = {
      method: method,
      headers: {
        'Authorization': this.getAuthHeader(relativeUrl, method, credentials),
        ...headers,
        // 'X-AppType' : 'bengga-app'
      },
      body: body
    }
    switch (method) {
      case 'DELETE': case 'PUT':
        return fetch(this.getFullUrl(relativeUrl), options)
      default:
        let a = fetch(this.getFullUrl(relativeUrl), options)
        if (asJson) {
          return a
            .then(function (response) {
              return response.json()
            })
        }
        else {
          return a;
        }
    }
  }

  request(method: FetchMethods, relativeUrl: string, body: ?any = null, headers: ?Object = {}, asJson: boolean = true): Promise<any> {
    return this._request(method, relativeUrl, this.credentials, body, headers, asJson);
  }
}