// @flow
import ApiClient from './ApiClient';

export default class ProductApiClient extends ApiClient {

  getProducts(start: number, limit: number): Promise<any> {
    const relativeUrl = `/products?start=${start}&limit=${limit}`;
    return this.request('GET', relativeUrl);
  }

  getProduct(id: string): Promise<any> {
    const relativeUrl = `/products/${id}`;
    return this.request('GET', relativeUrl);
  }
}