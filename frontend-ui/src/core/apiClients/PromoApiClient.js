// @flow
import ApiClient from './ApiClient';
import type { Promo } from '../Types';

export default class PromoApiClient extends ApiClient {

  getPromos(start: number, limit: number): Promise<any> {
    const relativeUrl = `/promos?start=${start}&limit=${limit}`;
    return this.request('GET', relativeUrl);
  }

  getPromo(id: string): Promise<any> {
    const relativeUrl = `/promos/${id}`;
    return this.request('GET', relativeUrl);
  }

  insertPromo(promo: Promo): Promise<any> {
    const relativeUrl = `/promos`;
    const params = {
      id: promo.id,
      gameId: promo.game.id,
      creatorId: promo.creatorId,
      items: promo.items.map(item => {
        return {
          category: item.category,
          promoTypeId: item.promoTypeId,
          value: item.value,
          productId: item.product.id
        }
      })
    }
    return this.request('POST', relativeUrl, params, { 'Content-Type': 'application/json' });
  }

  updatePromo(promo: Promo): Promise<any> {
    const relativeUrl = `/promos`;
    const params = {
      id: promo.id,
      gameId: promo.game.id,
      items: promo.items.map(item => {
        return {
          category: item.category,
          promoTypeId: item.promoTypeId,
          value: item.value,
          productId: item.product.id
        }
      })
    }
    return this.request('PUT', relativeUrl, params, { 'Content-Type': 'application/json' });
  }
}