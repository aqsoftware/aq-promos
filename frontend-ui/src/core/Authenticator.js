// @flow
import type { User } from './Types';
import type { IAuthenticator } from './Interfaces';

const dummyUser: User = {
  id: 'cNk-8AU1EeaGrCIACqyLYA',
  displayName: 'Jane Doe',
  avatarBig: 'http://lorempixel.com/512/512/people/1/',
  avatarSmall: 'http://lorempixel.com/160/160/people/1/',
  key: 'qRHhQnGee1AsPCpAylXCiohPgrVc35fe',
  algorithm: 'sha256'
}

export default class Authenticator implements IAuthenticator {

  currentUser: ?User;

  constructor(){
    this.currentUser = this.getUser();
  }

  isAuthenticated(): boolean {
    return this.currentUser != null && typeof(this.currentUser) !== 'undefined';
  }

  getUser(){
    if (!window.localStorage){
      return null;
    }
    else {
      try {
        return JSON.parse(window.localStorage.getItem("user"));
      }
      catch(e){
        return null;
      }      
    }
  }

  saveUser(user: ?User){
    if (window.localStorage) {
      window.localStorage.setItem("user", JSON.stringify(user));
      this.currentUser = user;
    }
  }

  login(cb: () => void){
    this.saveUser(dummyUser);
    setTimeout(cb, 100);
  }

  logout(cb: () => void){
    this.saveUser(null);
    setTimeout(cb, 100);
  }
}