// @flow

export type PromoItem = {
  id: string,
  name: string,
  imageBig: string,
  imageSmall: string,
  reserved: number,
  available: number,
  type: {
    id: number,
    name: string
  }
}

export type Promo = {
  id: string,
  engagement: Object,
  items: Array<Object>,
  promoCreator: Object
}

