// @flow
import { ApiClient } from '../../../core/apiClients';

export type RewardType = 'money' | 'freebie' | 'discount';
export type GiftStatus = 'available' | 'reserved' | 'given' | 'accepted' | 'claimed' | 'expired' | 'confirmed';

export const RewardTypeEnum = {
  'money': 0,
  'freebie': 1,
  'discount': 2
}

export const GiftStatusEnum = {
  'available': 0,
  'reserved': 1,
  'given': 2,
  'accepted': 3,
  'claimed': 4,
  'expired': 5,
  'confirmed': 6
}

export const GiftTypeEnum = {
  'freebie': 0,
  'coupon': 1,
  'stamp': 2,
  'giftcard': 3
}

export const BenggaMoneyTypeEnum = {
  'available': 0,
  'reserved': 1
}

export default class PromoApiClient extends ApiClient {

  getEngagementsWithPromo(isPublished: boolean, start: number, limit: number): Promise<any> {
    let relativeUrl = `/gift/promo/engagements?start=${start}&limit=${limit}&isPublished=${isPublished ? 'true' : 'false'}`;
    return this.request('GET', relativeUrl);
  }

  getGiftBalances(type: ?RewardType, start: number, limit: number, ignoreZeroBalances: ?boolean, status: ?GiftStatus): Promise<Array<any>> {
    let typeString = type == null ? '' : `&type=${RewardTypeEnum[type] - 1}`; //0: freebie, 1: coupon/discount
    let statusString = status == null ? '' : `&status=${GiftStatusEnum[status]}`;
    let ignoreString = ignoreZeroBalances != null ? `&ignoreZeroBalances=${ignoreZeroBalances ? 'true' : 'false'}` : '';
    const relativeUrl = `/gift/balances?start=${start}&limit=${limit}${typeString}${statusString}${ignoreString}`;
    return this.request('GET', relativeUrl);
  }

  getPromoTypes(): Promise<Array<any>> {
    const relativeUrl = '/gift/promo/types';
    return this.request('GET', relativeUrl);
  }
}