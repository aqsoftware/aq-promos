// @flow
import React from 'react';
import { Switch, Route } from 'react-router';
import ViewPromos from './views/ViewPromos';
import AddEditPromo from './views/AddEditPromo';
import AddEditPromoItem from './views/AddEditPromoItem';
import type { User } from '../../core/Types';
import type { Promo } from './core/Types';

type Props = {
  user: User,
  history: any,
  match: any
}

/*
Application state can be migrated to a Redux store if logic for maintaining state
gets complicated.
*/
type State = {
  promos: Array<Promo>,
  prizes: Array<Object>,
  promoTypes: Array<Object>,
  games: Array<Object>,
  current: {
    promo: ?Promo,
    category: number
  }
}

export default class Index extends React.Component<Props, State> {

  static defaultProps = {};

  constructor(props: Props) {
    super(props);
    this.state = {
      promos: [],
      prizes: [],
      promoTypes: [],
      games: [],
      current: {
        promo: null,
        category: -1
      }
    };
  }

  headerForComponent(component: Object) {
    return () => <h1>{component.title}<small>{component.description}</small></h1>
  }

  _onUpdateCurrentPromo(promo: Promo) {
    // TODO: Save current promo in application state
  }

  _onSavePromoItem(promoItem: Object) {
    // TODO: Add promoItem to current promo
  }

  _onSelectPromo(promo: Promo) {
    // TODO: Save selected promo in application state
    // which will be used by AddEditPromo component
    this.props.history.push(`/promos/${promo.id}`);
  }

  _onCancelAddEditPromo(){
    if (window.confirm('Are you sure you want to cancel?')) {
      this.props.history.push('/promos');
    }
  }

  _onAddPromoItem(category: number){
    // TODO: Show AddEditPromoItem component to add new 
    // promo item with given category
  }

  _getCurrentPromo(id: string){
    // TODO: Return promo with given id
  }

  _onCancelAddEditPromoItem(){    
    if (window.confirm('Are you sure you want to cancel?')) {
      if (this.state.current.promo) {
        this.props.history.push(`/promos/${this.state.current.promo.id}`);
      }
    }
  }

  render() {
    return (
      <Switch>
        <Route path='/promos' exact={true} render={props => <ViewPromos
          promos={this.state.promos}
          onSelectPromo={this._onSelectPromo.bind(this)}
          {...props}
        />
        } />
        <Route path='/promos/:id' exact={true} render={props => <AddEditPromo
          promo={this._getCurrentPromo(props.match.params.id)}
          onUpdate={this._onUpdateCurrentPromo.bind(this)}
          onAddPromoItem={this._onAddPromoItem.bind(this)}
          onCancel={this._onCancelAddEditPromo.bind(this)}
          {...props}
        />
        } />
        <Route path='/promos/:id/add/:category' exact={true} render={props => <AddEditPromoItem
          category={this.state.current.category}
          prizes={this.state.prizes}
          promoTypes={this.state.promoTypes}
          onSave={this._onSavePromoItem.bind(this)}
          onCancel={this._onCancelAddEditPromoItem.bind(this)}
          {...props}
        />
        } />
      </Switch>
    );
  }
}