// @flow
import React from 'react';
import type { PromoItem } from '../core/Types';

type Props = {
  category: number,
  gifts: Array<Object>,
  promoTypes: Array<Object>,
  onUpdate: ?(promoItem: PromoItem) => void,
  onSave: ?(promoItem: PromoItem) => void,
  onCancel: ?() => void
}

type State = {
  editedPromoItem: PromoItem
}

export default class AddEditPromoItem extends React.Component<Props, State> {
  
  static title = "Add Promo Item"

  static defaultProps = {};

  constructor(props: Props) {
    super(props);
    this.state = {
      editedPromoItem: {
        id: '',
        name: '',
        imageBig: '',
        imageSmall: '',
        reserved: 0,
        available: 0,
        type: {
          id: 0,
          name: ''
        }
      }
    };
  }

  _onUpdateCb() {
    if (this.props.onUpdate) this.props.onUpdate(this.state.editedPromoItem);
  }

  _onCancel() {
    if (this.props.onCancel) this.props.onCancel();    
  }

  _validate(promoItem: PromoItem): ?string {
    // TODO: Validate if promoItem fields are valid or not
    // Return an error message if validation failed, or null if succeeds
    return null;
  }

  _onSave(){
    const promoItem = this.state.editedPromoItem;
    const validationMessage = this._validate(promoItem);
    if (!validationMessage) {
      if (this.props.onSave) {
        this.props.onSave(promoItem);
      }
    } 
    else {
      alert(validationMessage);
    }    
  }

  render() {

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>{AddEditPromoItem.title}<small></small></h1>
        </section>
        <section className="content container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="box box-solid">
                <div className="box-header with-border">
                  <h3 className="box-title">Summary</h3>
                </div>
                <div className="box-body">
                  <p>Things that should be displayed here</p>
                  <ol>
                    <li>
                      <b>UI to add/edit promo item fields</b> - Put relevant form elements to input promo fields.
                      Refer to <a href="https://adminlte.io/themes/AdminLTE/pages/forms/general.html">AdminLTE's form UI elements</a> for more information
                    </li>
                    <li>
                      <b>Buttons to save/cancel editing</b>
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>  
    );
  }
}