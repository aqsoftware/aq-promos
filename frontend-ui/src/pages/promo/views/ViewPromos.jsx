// @flow
import React from 'react';
import type { Promo } from '../core/Types';

type Props = {
  promos: Array<Promo>,
  onSelectPromo: ?(promo: Promo) => void
}

type State = {
}

export default class ViewPromos extends React.Component<Props, State> {
  static title: string = "All Promos";

  static defaultProps = {};

  constructor(props: Props) {
    super(props);
    this.state = {
    };
  }

  _onClickPromo(item: Promo) {
    if (this.props.onSelectPromo) {
      this.props.onSelectPromo(item);
    }
  }

  render() {

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>{ViewPromos.title}<small></small></h1>
        </section>
        <section className="content container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="box box-solid">
                <div className="box-header with-border">
                  <h3 className="box-title">Summary</h3>
                </div>
                <div className="box-body">
                  <p>Things that should be displayed here</p>
                  <ol>
                    <li><b>List of all promos</b> - User can select a promo and will redirected to <code>/promo/:id</code> via <code>_onClickPromo</code> for editing.</li>
                    <li><b>Way to add a new promo</b> - Add an <code>Add</code> button which will redirect to <code>/promo/add</code>.</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>      
    );
  }
}