// @flow
import React from 'react';
import type { Promo } from '../core/Types';

type Props = {
  promo: Promo,
  onUpdate: ?(promo: Promo) => void,
  onAddPromoItem: ?(category: number) => void,
  onCancel: ?() => void
}

type State = {
  editedPromo: Promo
}

export default class AddEditPromo extends React.Component<Props, State> {
  static title: string = "Edit Promo";
  
  static defaultProps = {};

  constructor(props: Props) {
    super(props);
    this.state = {
      editedPromo: props.promo
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    this.setState({
      editedPromo: nextProps.promo
    });
  }

  _onUpdateCb(){
    if (this.props.onUpdate) this.props.onUpdate(this.state.editedPromo);
  }

  _onCancel(){
    if (this.props.onCancel) this.props.onCancel();    
  }

  _onAddPromoItem(category: number){
    if (this.props.onAddPromoItem) {
      this.props.onAddPromoItem(category);
    }
  }

  _onDeletePromoItem(category: number, id: string) {
    if (window.confirm(`Are you sure you want to delete this promo item? ${category} ${id}`)) {
      // Remove item with given id
      const items = this.state.editedPromo.items.map((item, itemIndex) => {
        if (item.category !== category){
          return item;
        }
        else {
          const giftItems = item.giftItems.filter( giftItem => giftItem.id !== id);
          return {
            ...item,
            giftItems: giftItems
          }
        }
      });
      this.setState({
        editedPromo: {
          ...this.props.promo,
          items: items
        }
      }, this._onUpdateCb);
    }
  }

  render() {
    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>{AddEditPromo.title}<small></small></h1>
        </section>
        <section className="content container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="box box-solid">
                <div className="box-header with-border">
                  <h3 className="box-title">Summary</h3>
                </div>
                <div className="box-body">
                  <p>Things that should be displayed here</p>
                  <ol>
                    <li>
                      <b>UI to add/edit promo fields</b> - Put relevant form elements to input promo fields. 
                      Refer to <a href="https://adminlte.io/themes/AdminLTE/pages/forms/general.html">AdminLTE's form UI elements</a> for more information
                    </li>
                    <li>
                      <b>UI to display promo items</b> - Incorporate tables to group promo items into categories (Major, Minor, Consolation, Giveaway).
                    </li>
                    <li>
                      <b>UI elements to add/remove individual promo items</b> - Add button to add a new promo item or edit an existing one
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>  
    );
  }
}