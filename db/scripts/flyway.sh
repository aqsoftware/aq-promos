#!/usr/bin/env bash
if [ "$#" -ne 5 ]; then
  echo ""
  echo "Usage: ./flyway.sh sql_migration_path db_name user password flyway_command"
  echo ""
  exit 1
fi

docker run -it --rm --link postgres:postgres -v $1:/flyway/sql shouldbee/flyway -url=jdbc:postgresql://postgres/$2 -user=$3 -password=$4 $5