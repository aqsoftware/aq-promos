
copy 
(select id,create_date,last_modified_date,display_name,avatar_big,avatar_small,user_type 
 from users)
TO '/var/lib/postgresql/data/users.csv';


copy 
(select id,create_date,last_modified_date,title,image_big,image_small,source_id,publish_date
 from engagements where source_id in ('70d93ef0-0535-11e6-86ac-22000aac8b60','30485860-caf6-11e3-91ec-123139260fba'))
TO '/var/lib/postgresql/data/games.csv';


copy
(select p.id,p.create_date,p.last_modified_date,p.name,p.image_big,p.image_small,p.product_type_id,p.creator_id,p.is_active,p.bm_conversion, count(pi.id) balance
from products p 
inner join product_items pi on pi.product_id=p.id
where pi.owner_id in ('70d93ef0-0535-11e6-86ac-22000aac8b60','30485860-caf6-11e3-91ec-123139260fba')
and pi.status_id = 0
group by p.id
)
TO '/var/lib/postgresql/data/products.csv';

copy users from '/var/lib/postgresql/data/users.csv';     
copy games from '/var/lib/postgresql/data/games.csv';     
copy products from '/var/lib/postgresql/data/products.csv';  