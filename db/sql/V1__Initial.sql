-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
    id uuid NOT NULL,
    create_date timestamp without time zone NOT NULL,
    last_modified_date timestamp without time zone NOT NULL,
    display_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    avatar_big character varying(1000) COLLATE pg_catalog."default",
    avatar_small character varying(1000) COLLATE pg_catalog."default",
    user_type integer NOT NULL DEFAULT 0,
    CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to aq;


-- Table: public.products

-- DROP TABLE public.products;

CREATE TABLE public.products
(
    id uuid NOT NULL,
    create_date timestamp without time zone NOT NULL,
    last_modified_date timestamp without time zone NOT NULL,
    name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    image_big character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    image_small character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    product_type_id integer NOT NULL,
    creator_id uuid NOT NULL,
    is_active boolean NOT NULL DEFAULT true,
    bm_conversion integer,
    balance integer NOT NULL,
    CONSTRAINT products_pkey PRIMARY KEY (id)
    -- CONSTRAINT products_creator_id_fkey FOREIGN KEY (creator_id)
    --     REFERENCES public.users (id) MATCH SIMPLE
    --     ON UPDATE NO ACTION
    --     ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.products
    OWNER to aq;    

CREATE TABLE public.games (
  id UUID NOT NULL PRIMARY KEY,
  create_date TIMESTAMP NOT NULL,
  last_modified_date TIMESTAMP NOT NULL,
  title VARCHAR(100) NOT NULL,
  image_big VARCHAR(1000) NOT NULL,
  image_small VARCHAR(1000) NOT NULL,  
  source_id UUID NOT NULL,
  publish_date TIMESTAMP NOT NULL
);    

-- Table: public.promos

-- DROP TABLE public.promos;

CREATE TABLE public.promos
(
    id uuid NOT NULL,
    create_date timestamp with time zone NOT NULL,
    last_modified_date timestamp with time zone NOT NULL,
    game_id uuid NOT NULL,
    promo_creator_id uuid NOT NULL,
    processed boolean NOT NULL DEFAULT false,
    CONSTRAINT promos_pkey PRIMARY KEY (id),
    CONSTRAINT promos_game_id_fkey FOREIGN KEY (game_id)
        REFERENCES public.games (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.promos
    OWNER to aq;


-- Table: public.promo_items

-- DROP TABLE public.promo_items;

CREATE TABLE public.promo_items
(
    id uuid NOT NULL,
    create_date timestamp with time zone NOT NULL,
    last_modified_date timestamp with time zone NOT NULL,
    promo_id uuid NOT NULL,
    category smallint NOT NULL,
    promo_type_id integer NOT NULL,
    value integer NOT NULL,
    product_id uuid NOT NULL,
    CONSTRAINT promo_items_pkey PRIMARY KEY (id),
    CONSTRAINT promo_items_product_id_fkey FOREIGN KEY (product_id)
        REFERENCES public.products (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT promo_items_promo_id_fkey FOREIGN KEY (promo_id)
        REFERENCES public.promos (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.promo_items
    OWNER to aq;



CREATE SEQUENCE tmp_promos_id_seq;

CREATE TABLE public.tmp_promos (
    id INT NOT NULL DEFAULT nextval('tmp_promos_id_seq'),
    game_id uuid NOT NULL,
    game_title VARCHAR(100) NOT NULL,
    promo_creator_id uuid NOT NULL,
    game_source_id uuid NOT NULL,
    processed boolean NOT NULL DEFAULT false,
    category smallint NOT NULL,
    promo_type_id integer NOT NULL,
    product_id uuid NOT NULL,
    value integer NOT NULL,
    PRIMARY KEY (id)
);

CREATE SEQUENCE reserved_products_id_seq;

CREATE TABLE reserved_products(
    id INT NOT NULL DEFAULT nextval('reserved_products_id_seq'),
    promo_id UUID NOT NULL REFERENCES promos(id) ,
    product_id UUID NOT NULL REFERENCES products(id),
    product_item_id UUID NOT NULL
);