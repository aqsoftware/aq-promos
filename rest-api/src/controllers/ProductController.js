// @flow
import { ProductRepository } from '../core';
import { Utils } from '../core';

const LIMIT = 20;

export default class ProductController {

  productRepo: ProductRepository;

  constructor(productRepo: ProductRepository){
    this.productRepo = productRepo;
  }

  getProducts(req: any, res: any, next: any) {
    const start = req.query.start || 0;
    const limit = req.query.limit || LIMIT;

    this.productRepo.getProducts(start, limit)
      .then(result => res.send(result))
      .catch(next);
  }

  getProduct(req: any, res: any, next: any) {
    try {
      const id = Utils.base64ToUuid(req.params.id);

      this.productRepo.getProduct(id)
        .then(result => res.send(result))
        .catch(next);
    }
    catch(e) {
      res.status(400).send(`Invalid id: ${req.params.id}`);
    }
  }
}