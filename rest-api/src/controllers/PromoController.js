// @flow
import { PromoRepository } from '../core';
import { Utils } from '../core';

const LIMIT = 20;

export default class PromoController {

  promoRepo: PromoRepository;

  constructor(PromoRepo: PromoRepository) {
    this.promoRepo = PromoRepo;
  }

  getPromos(req: any, res: any, next: any) {
    const start = req.query.start || 0;
    const limit = req.query.limit || LIMIT;

    this.promoRepo.getPromos(start, limit)
      .then(result => res.send(result))
      .catch(next);
  }

  getPromo(req: any, res: any, next: any) {
    this.promoRepo.getPromo(req.params.id)
      .then(result => res.send(result))
      .catch(next);
  }

  insertPromo(req: any, res: any, next: any) {
    this.promoRepo.insert(req.body)
      .then(result => res.send(result))
      .catch(e => res.status(400).json({error: e}));
  }

  updatePromo(req: any, res: any, next: any) {
    this.promoRepo.update(req.body)
      .then(result => res.send(result))
      .catch(e => res.status(400).json({ error: e }));
  }
}