// @flow
import { Client } from 'pg';
import { Utils } from '..';
import type { Product } from '../Types';

export default class ProductRepository {

  client: Client;

  constructor(client: Client) {
    this.client = client;
  }

  static mapper(item: any): Product {
    return {
      id: Utils.uuidToBase64(item.id),
      createDate: item.create_date,
      lastModifiedDate: item.last_modified_date,
      name: item.name,
      imageBig: item.image_big,
      imageSmall: item.image_small,
      productTypeId: item.product_type_id,
      creatorId: Utils.uuidToBase64(item.creator_id),
      isActive: item.is_active,
      bmConversion: item.bm_conversion,
      balance: item.balance
    }
  }

  insertProduct(id: string, createDate: string, lastModifiedDate: string, name: string, 
    imageBig: string, imageSmall: string, productType: number, sourceId: string, bmConversion: number,
    balance: number): Promise<Product> {
    
    return this.client.query(`INSERT INTO products VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)`,
        [
          Utils.base64ToUuid(id),
          createDate,
          lastModifiedDate,
          name,
          imageBig,
          imageSmall,
          productType,
          Utils.base64ToUuid(sourceId),
          true,
          bmConversion,
          balance
        ]
      )
      .then(_ => 
        ProductRepository.mapper(
        {
          id: Utils.base64ToUuid(id),
          create_date: createDate,
          last_modified_date: lastModifiedDate,
          name: name,
          image_big: imageBig,
          image_small: imageSmall,
          product_type_id: productType,
          creator_id: Utils.base64ToUuid(sourceId),
          is_active: true,
          bm_conversion: bmConversion,
          balance: balance
        }
      ));
  }

  getProduct(id: string): Promise<?Product> {
    return this.client.query(`SELECT * FROM products WHERE id=$1`, [Utils.base64ToUuid(id)])
      .then(res => {
        if (res.rows.length) {
          return res.rows.map(ProductRepository.mapper)[0];
        }
        else return null;
      })
  }

  getProducts(start: number, limit: number): Promise<Array<Product>> {
    return this.client.query(`SELECT * FROM products ORDER BY name ASC LIMIT $1 OFFSET $2`, [limit, start])
      .then(res => {
        return res.rows.map(ProductRepository.mapper);
      })
  }
}