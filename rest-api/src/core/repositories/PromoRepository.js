// @flow
import { Client } from 'pg';
import { Utils, Validator } from '..';
import { ProductRepository } from '.';
import { GameRepository } from '.';
import type { 
  Game, 
  Promo, 
  PromoItem, 
  Product 
} from '../Types';

const BASE_QUERY = `
SELECT *, g.create_date game_create_date, g.last_modified_date game_last_modified_date,
p.id promo_id,
g.image_big game_image_big, g.image_small game_image_small,
pr.create_date product_create_date, pr.last_modified_date product_last_modified_date,
pr.image_big product_image_big, pr.image_small product_image_small,
pi.id promo_item_id
FROM ($PROMOS_QUERY) p 
INNER JOIN games g ON g.id = p.game_id 
INNER JOIN promo_items pi on pi.promo_id=p.id 
LEFT JOIN products pr ON pr.id = pi.product_id 
`

export default class PromoRepository {

  client: Client;

  constructor(client: Client) {
    this.client = client;
  }

  static mapper(items: Array<Object>): Array<Promo> {
    let currentId: ?string = null;
    let promos: Array<Promo> = [];
    let currentPromo: ?Promo = null;
    let promoItems: Array<PromoItem> = [];

    for (let i = 0; i < items.length; i++) {
      const item = items[i];

      if (!currentPromo) {
        currentPromo = {
          id: Utils.uuidToBase64(item.promo_id),
          createDate: item.create_date,
          lastModifiedDate: item.last_modified_date,
          creatorId: Utils.uuidToBase64(item.promo_creator_id),
          game: GameRepository.mapper({
            id: item.game_id,
            create_date: item.game_create_date,
            last_modified_date: item.game_last_modified_date,
            title: item.title,
            image_big: item.game_image_big,
            image_small: item.game_image_small
          }),
          items: []
        }
      }

      if (currentPromo.id !== Utils.uuidToBase64(item.promo_id)) {
        currentPromo.items = promoItems;
        promoItems = [];
        promos.push(currentPromo)
        currentPromo = null;
      }

      const promoItem: PromoItem = {
        id: Utils.uuidToBase64(item.promo_item_id),
        createDate: item.create_date,
        lastModifiedDate: item.last_modified_date,
        category: item.category,
        promoTypeId: item.promo_type_id,
        value: item.value,
        product: typeof item.product_id === 'string' ? ProductRepository.mapper({
          id: item.product_id,
          create_date: item.product_create_date,
          last_modified_date: item.product_last_modified_date,
          name: item.name,
          image_big: item.product_image_big,
          image_small: item.product_image_small,
          product_type_id: item.product_type_id,
          creator_id: item.creator_id,
          is_active: item.is_active,
          bm_conversion: item.bm_conversion,
          balance: item.balance,
        }) : null,
        quantity: item.quantity
      }
      promoItems.push(promoItem);   

      
    }

    if (currentPromo){
      currentPromo.items = promoItems;
      promos.push(currentPromo)
    }

    return promos;
  }

  validate(params: Object, forUpdate: boolean = false): ?string {
    let error: ?string = null;

    error = Validator.isDefined(error, 'Parameters', params);
    error = Validator.isId(error, 'id', params.id);
    error = Validator.isId(error, 'gameId', params.gameId);
    if (!forUpdate) {
      error = Validator.isId(error, 'creatorId', params.creatorId);
    }
    error = Validator.isArray(error, 'items', params.items, false);

    if (error === null) {
      for (let i = 0; i < params.items.length; i++) {
        const item = params.items[i];
        if (error !== null) {
          break;
        }
        error = Validator.isObject(error, 'item entries', item);
        error = Validator.isInteger(error, 'item.category', item.category, true, 0, 2);
        error = Validator.isInteger(error, 'item.promoTypeId', item.promoTypeId, true, 0, 2);

        error = Validator.either(error, ['item.value', 'item.productId'], [item.value, item.productId], (index) => {
          switch (index) {
            case 0:
              return Validator.isInteger(error, 'item.value', item.value, true);
            default:
              return Validator.isId(error, 'item.productId', item.productId);
          }
        });        
        error = Validator.isInteger(error, 'item.quantity', item.quantity);
      }
    }
    return error;
  }

  insert(params: Object): Promise<?Promo> {
    const error = this.validate(params);

    if (error) {
      return new Promise((_, reject) => reject(error));
    }
    else {
      const currentDate = new Date().toISOString();
      return this.insertPromo(params.id, currentDate, currentDate, params.gameId,
        params.creatorId, params.items);
    }
  }

  update(params: Object): Promise<?Promo> {
    const error = this.validate(params, true);

    if(error) {
      return new Promise((_, reject) => reject(error));
    }
    else {
      const currentDate = new Date().toISOString();
      return this.updatePromo(params.id, currentDate, params.gameId, params.items);
    } 
  }

  insertPromo(id: string, createDate: string, lastModifiedDate: string, gameId: string,
    creatorId: string, items: Array<{category: number, promoTypeId: number, value: ?number, productId: ?string, quantity: number}>): Promise<?Promo> {

    return this.client.query('BEGIN')
      .then(_ => this.client.query(`INSERT INTO promos VALUES($1,$2,$3,$4,$5,$6)`,
        [
          Utils.base64ToUuid(id),
          createDate,
          lastModifiedDate,
          Utils.base64ToUuid(gameId),
          Utils.base64ToUuid(creatorId),
          false
        ]
      ))
      .then(_ => 
        Promise.race(
          items.map(item => {
            const promoItemId = Utils.generateId();
            return this.client.query(`INSERT INTO promo_items VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9)`,
              [
                Utils.base64ToUuid(promoItemId),
                createDate,
                lastModifiedDate,
                Utils.base64ToUuid(id),
                item.category,
                item.promoTypeId,
                item.value,
                typeof item.productId === 'string' ? Utils.base64ToUuid(item.productId) : null,
                item.quantity                
              ]
            );
          })
        )
      )
      .then(_ => this.client.query('COMMIT'))
      .then(_ => this.getPromo(id))
      .catch(err => {
        this.client.query('ROLLBACK');
        console.error(`Unable to insert new promo: ${JSON.stringify(err)}`);
        throw `An error occured while inserting a new promo: ${err.detail}`;
      });
  }
  
  updatePromo(id: string, lastModifiedDate: string, gameId: string, items: Array<{ category: number, promoTypeId: number, value: ?number, productId: ?string, quantity: number }>): Promise<?Promo> {
      return this.getPromo(id)
        .then(promo => {
          if(promo === null) {
            throw `Promo with id=${id} not found`;
          }
        })
        .then(_ => this.client.query('BEGIN')) 
        .then(_ => this.client.query(`DELETE FROM promo_items WHERE promo_id = $1`, [Utils.base64ToUuid(id)]))
        .then(_ => this.client.query(`
          UPDATE promos 
          SET last_modified_date=$1, game_id=$2
          WHERE id=$3
        `,[
            lastModifiedDate,
            Utils.base64ToUuid(id),
            Utils.base64ToUuid(gameId)
        ]))
        .then(_ =>
          Promise.race(
            items.map(item => {
              const promoItemId = Utils.generateId();
              return this.client.query(`INSERT INTO promo_items VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9)`,
                [
                  Utils.base64ToUuid(promoItemId),
                  lastModifiedDate,
                  lastModifiedDate,
                  Utils.base64ToUuid(id),
                  item.category,
                  item.promoTypeId,
                  item.value,
                  typeof item.productId === 'string' ? Utils.base64ToUuid(item.productId) : null,
                  item.quantity                
                ]
              );
            })
          )
        )
        .then(_ => this.client.query('COMMIT'))
        .then(_ => this.getPromo(id))
        .catch(err => {
          this.client.query('ROLLBACK');
          console.error(`Unable to update new promo: ${JSON.stringify(err)}`);
          throw `An error occured while updating promo with id=${id}: ${err.detail}`;
        });
    }
  

  getPromo(id: string): Promise<?Promo> {
    const query = BASE_QUERY.replace('$PROMOS_QUERY', 'SELECT * FROM promos ORDER BY last_modified_date') + `WHERE p.id=$1`;
    try {
      return this.client.query(query, [Utils.base64ToUuid(id)])
        .then(res => {
          if (res.rows.length) {
            return PromoRepository.mapper(res.rows)[0];
          }
          else {
            return null;
          }
        })
    }
    catch(err) {
      console.error(`Unable to get promo: ${JSON.stringify(err)}`);
      return new Promise((_, reject) => reject(err));
    }      
  }

  getPromos(start: number, limit: number): Promise<Array<Promo>> {
    const query = BASE_QUERY.replace('$PROMOS_QUERY', 'SELECT * FROM promos ORDER BY last_modified_date DESC LIMIT $1 OFFSET $2') + `ORDER BY p.last_modified_date`;
    console.log(`${query}`);
    return this.client.query(query, [limit, start])
      .then(res => {
        if (res.rows.length) {
          return PromoRepository.mapper(res.rows);
        }
        else {
          return [];
        }
      });
  }
}