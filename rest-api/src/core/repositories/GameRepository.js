// @flow
import { Client } from 'pg';
import { Utils } from '..';
import type { Game } from '../Types';

export default class GameRepository {

  client: Client;

  constructor(client: Client) {
    this.client = client;
  }

  static mapper(item: any): Game {
    return {
      id: Utils.uuidToBase64(item.id),
      createDate: item.create_date,
      lastModifiedDate: item.last_modified_date,
      title: item.name,
      imageBig: item.image_big,
      imageSmall: item.image_small
    }
  }

  getGames(start: number, limit: number): Promise<Array<Game>> {
    return this.client.query(`SELECT * FROM games ORDER BY name ASC LIMIT $1 OFFSET $2`, [limit, start])
      .then(res => {
        return res.rows.map(GameRepository.mapper);
      })
  }
}