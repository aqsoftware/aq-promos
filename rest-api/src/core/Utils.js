// @flow
import base64js from 'base64-js';
import base64Url from 'base64-url';
import UUID from 'uuid-js';

class Utils {
  
  base64ToUuid(value: string) {
    const bytes = base64js.toByteArray(base64Url.unescape(value));
    return UUID.fromBytes(bytes).toString();
  } 

  uuidToBase64(value: string){
    const bytes = UUID.fromURN(value).toBytes();
    return base64Url.escape(base64js.fromByteArray(bytes));
  }

  generateId(): string {
    const bytes = UUID.create(1).toBytes();
    return base64Url.escape(base64js.fromByteArray(bytes));
  }

  arrayToCSV(array: Array<Object>, options: {
    columnDelimeter?: string,
    lineDelimiter?: string
  } = {}): ?string {
    if (array == null || !array.length){
      return null;
    }

    let columnDelimeter = options && options.columnDelimeter || ',';
    let lineDelimiter = options && options.lineDelimiter || '\n';
    let result = '';

    const keys = Object.keys(array[0]);
    result = keys.map((item) => `"${item}"`).join(columnDelimeter) + lineDelimiter;

    array.forEach((item) => {
      let hasColumnDelimeter = false;
      keys.forEach((key) => {
        if (hasColumnDelimeter) {
          result += columnDelimeter;
        }
        let normalizedItem = item[key];
        switch (typeof normalizedItem) {
          case 'undefined':
          case 'object':
          case 'function':
            normalizedItem = `""`;  
            break;
          case 'boolean':
            normalizedItem = normalizedItem ? '1' : '0';
            break;
          case 'number':
            // No modification necessary
            break;
          default:
            // Surround with double quotes
            normalizedItem = `"${normalizedItem}"`;
            break;
        }
        result += normalizedItem;
        hasColumnDelimeter = true;
      });
      result += lineDelimiter;
    });

    return result;

  }
}

export default Utils = new Utils();