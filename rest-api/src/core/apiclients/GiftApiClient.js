// @flow
import ApiClient from './ApiClient';

export default class GiftApiClient extends ApiClient {

  getGiftBalances(start: number, limit: number): Promise<any> {
    const relativeUrl = `/gift/balances?start=${start}&limit=${limit}`;
    return this.request('GET', relativeUrl);
  }

  getPromoCategories(): Promise<any> {
    const relativeUrl = `/gift/promo/categories`;
    return this.request('GET', relativeUrl);
  }

  reserve(productId: string, quantity: number, tag: string) {
    const relativeUrl = `/gift/reserve`;
    const params = {
      productId: productId,
      quantity: quantity,
      tag: tag
    }
    return this.request('POST', relativeUrl, params, {'Content-Type': 'application/json'});
  }

  unreserve(productItemIds: Array<string>) {
    const relativeUrl = `/gift/unreserve`;
    const params = {
      productItemIds: productItemIds
    }
    return this.request('POST', relativeUrl, params, { 'Content-Type': 'application/json' });
  }
}