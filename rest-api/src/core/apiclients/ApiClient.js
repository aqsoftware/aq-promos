// @flow
import fetch from 'node-fetch';
import Hawk from 'hawk';

type FetchMethods = 'DELETE' | 'GET' | 'HEAD' | 'OPTIONS' | 'PATCH' | 'POST' | 'PUT';

export default class ApiClient {
  baseUrl: string;
  credentials: any;
  systemCredentials: any;

  constructor(baseUrl: string, credentials: any, systemCredentials: any) {
    this.baseUrl = baseUrl;
    this.credentials = credentials;
    this.systemCredentials = systemCredentials;
  }

  getAuthHeader(url: string, method: string, credentials: any): string {
    return Hawk.client.header(this.getFullUrl(url), method, { credentials: credentials, ext: 'aq-promos' }).field;
  }

  getFullUrl(url: string): string {
    return `${this.baseUrl}${url}`;
  }
  _request(method: FetchMethods, relativeUrl: string, credentials: any, body: ?any = null, headers: ?Object = {}, asJson: boolean = true): Promise<any> {
    console.info(`${method} ${this.getFullUrl(relativeUrl)}`);
    // console.info(`Auth = ${this.getAuthHeader(relativeUrl, method, credentials)}`);

    let actualBody: ?string = null;
    if (body) {
      actualBody = '' + JSON.stringify(body);
    }
    
    let options = {
      method: method,
      headers: {
        'Authorization': this.getAuthHeader(relativeUrl, method, credentials),
        ...headers,
        // 'X-AppType' : 'bengga-app'
      },
      body: actualBody
    }
    switch (method) {
      case 'DELETE': case 'PUT':
        return fetch(this.getFullUrl(relativeUrl), options)
      default:
        return fetch(this.getFullUrl(relativeUrl), options)
          .then(async (response) => {
            if (response.ok) {
              if (asJson) {
                return response.json();
              }
              else {
                return response;
              }
            }
            else {
              throw {
                status: response.status,
                statusText: response.statusText,
                body: await response.json()
              }
            }
          });
        // if (asJson) {
        //   return a
        //     .then(function (response) {
        //       if (response.ok) {
        //         return response.json()
        //       }
        //       else {
        //         throw {
        //           status: response.status,
        //           statusText: response.statusText,
        //           body: response.json()
        //         }
        //       }
        //     })
        // }
        // else {
        //   return a.then((response) => {
        //     if (response.ok) {
        //       return response.json()
        //     }
        //     else {
        //       throw {
        //         status: response.status,
        //         statusText: response.statusText,
        //         body: response.json()
        //       }
        //     }
        //   });
        // }
    }
  }

  request(method: FetchMethods, relativeUrl: string, body: ?any = null, headers: ?Object = {}, asJson: boolean = true): Promise<any> {
    return this._request(method, relativeUrl, this.credentials, body, headers, asJson);
  }
}