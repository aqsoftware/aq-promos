// @flow
import ApiClient from './ApiClient';

export default class GameApiClient extends ApiClient {

  getGames(userId: string, start: number, limit: number): Promise<any> {
    const relativeUrl = `/engagement?start=${start}&limit=${limit}&other=${userId}`;
    return this.request('GET', relativeUrl);
  }
}