// @flow
import { 
  EngagementApiClient,
  GiftApiClient
} from '.';

export type Config = {
  user: {
    id: string,
    key: string
  },
  aq: {
    endpoint: string
  },
  db: {
    database: string,
    host: string,
    user: string,
    password: string
  },
  rest: {
    port: number
  }
}

export type Globals = {
  clients: {
    engagement: EngagementApiClient,
    gift: GiftApiClient
  },
  user: {
    id: string,
    key: string
  },
  aq: {
    endpoint: string
  },
  db: {
    database: string,
    host: string,
    user: string,
    password: string
  }
}

export type Product = {
  id: string,
  createDate: string,
  lastModifiedDate: string,
  name: string,
  imageBig: ?string,
  imageSmall: ?string,
  productTypeId: number,
  creatorId: string,
  isActive: boolean,
  bmConversion: number,
  balance: number
}

export type Game = {
  id: string,
  createDate: string,
  lastModifiedDate: string,
  title: string,
  imageBig: string,
  imageSmall: string
}

export type PromoItem = {
  id: string,
  createDate: string,
  lastModifiedDate: string,
  category: number,
  promoTypeId: number,
  value: ?number,
  product: ?Product,
  quantity: number
}

export type Promo = {
  id: string,
  createDate: string,
  lastModifiedDate: string,
  game: Game,
  creatorId: string,
  items: Array<PromoItem>
}