// @flow
import Utils from './Utils';

class Validator {

  isDefined(error: ?string, fieldName: string, value: any): ?string {
    error = error !== null ? error : typeof value === 'undefined' ? `${fieldName} must be defined` : null;
    return error;
  }

  validateType(error: ?string, fieldName: string, value: any, type: string, errorMessage: string): ?string {
    error = this.isDefined(error, fieldName, value);

    if (type === 'array') {
      error = error !== null ? error : !Array.isArray(value) ? `${fieldName} must be an ${type}` : null;
    }
    else {
      error = error !== null ? error : typeof value !== type ? `${fieldName} must be a ${errorMessage}` : null;
    }
    return error;
  }

  isObject(error: ?string, fieldName: string, value: any): ?string {
    return this.validateType(error, fieldName, value, 'object', 'valid object');
  }

  isArray(error: ?string, fieldName: string, value: any, canBeEmpty: boolean = true): ?string {
    error = this.validateType(error, fieldName, value, 'array', 'array');
    if (!canBeEmpty) {
      error = error !== null ? error : value.length === 0 ? `${fieldName} must not be empty.` : null;
    }
    return error;
  }

  isNumber(error: ?string, fieldName: string, value: any): ?string {
    return this.validateType(error, fieldName, value, 'number', 'valid number');
  }

  isInteger(error: ?string, fieldName: string, value: any, isPositive: boolean = false, min: ?number = null, max: ?number = null): ?string {
    error = this.isNumber(error, fieldName, value);
    error = error !== null ? error : Math.trunc(parseFloat(value)) !== parseFloat(value) ? `${fieldName} must be an integer` : null;
    if (isPositive) {
      error = error !== null ? error : parseInt(value) < 0 ? `${fieldName} must be a positive integer` : null;
    }
    if (error === null && min !== null){
      error = error !== null ? error : parseInt(value) < parseInt(min) ? `${fieldName} must be a greater than or equal to ${parseInt(min)}` : null;
    }
    if (error === null && max !== null){
      error = error !== null ? error : parseInt(value) > parseInt(max) ? `${fieldName} must be a less than or equal to ${parseInt(max)}` : null;
    }
    return error;
  }

  isFloat(error: ?string, fieldName: string, value: any, isPositive: boolean = false, min: ?number = null, max: ?number = null): ?string {
    error = this.isNumber(error, fieldName, value);
    if (isPositive) {
      error = error !== null ? error : parseFloat(value) < 0 ? `${fieldName} must be a positive number` : null;
    }
    if (error === null && min !== null) {
      error = error !== null ? error : parseFloat(value) < parseFloat(min) ? `${fieldName} must be a greater than or equal to ${parseFloat(min)}` : null;
    }
    if (error === null && max !== null) {
      error = error !== null ? error : parseFloat(value) > parseFloat(max) ? `${fieldName} must be a less than or equal to ${parseFloat(max)}` : null;
    }
    return error;
  }

  isId(error: ?string, fieldName: string, value: any): ?string {
    error = this.isDefined(error, fieldName, value);
    error = this.validateType(error, fieldName, value, 'string', 'valid URL-safe, Base64-encoded UUID string');
    try {
      if (error === null) {
        Utils.base64ToUuid(value);
      }
    }
    catch (_) {
      error = `${fieldName} must be a valid URL-safe, Base64-encoded UUID string`;
    }

    return error;
  }

  either(error: ?string, fieldNames: Array<string>, values: Array<any>, validationFunction: (index: number) => ?string): ?string {
    var errors: Array<?string> = [];

    for (let i = 0; i < values.length; i++){
      const value = values[i];
      if (typeof value !== 'undefined') {
        errors.push(validationFunction(i));
      }
    }

    var numSuccess = 0;
    for (let i = 0; i < errors.length; i++) {
      if(error === null && errors[i] !== null) {
        error = errors[i];
      }
      else {
        numSuccess += 1;
      }
    }
    if (error) {
      return error;
    }
    if (numSuccess !== 1) {
      return `Only one of the following fields should be present: ${fieldNames.join(', ')}`;
    }
    return null;
  }
}

export default Validator = new Validator();