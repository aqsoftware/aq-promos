// @flow
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import fs from 'fs';
import os from 'os';
import yaml from 'js-yaml';
import { Client } from 'pg';
import productRoutes from './routes/ProductRoutes';
import promoRoutes from './routes/PromoRoutes';
import type { Globals, Config } from './core/Types';

var configFile: ?string;
if (process.env.NODE_ENV === 'devt') {
  configFile = './config.yaml';
}
else {
  configFile = `${os.homedir()}/.aq/config.yaml`;
}
const config: Config = yaml.safeLoad(fs.readFileSync(configFile, 'utf-8'));
const client = new Client(config.db);

const app = express();

app.use(cors());
app.use(bodyParser.json());

productRoutes(app, client);
promoRoutes(app, client);

client.connect()
  .then(_ => {
    app.listen(config.rest.port, () => {
      console.log('AQ Promo RESTful API server started on: ' + config.rest.port);
    });
  })
  .catch(e => {
    console.log(`Shutting down... ${e}`);
    client.end();
  });




