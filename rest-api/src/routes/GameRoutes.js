// @flow
import Express from 'express';
import {
  GameRepository
} from '../core/repositories';
import {
  GameController
} from '../controllers';

export default function setupRoute(app: Express, client: Client) {

  const gameRepository = new GameRepository(client);
  const gameController = new GameController(gameRepository);

  app.route('/games')
    .get(gameController.getGames.bind(gameController))
} 