// @flow
import Express from 'express';
import {
  ProductRepository
} from '../core/repositories';
import { 
  ProductController 
} from '../controllers';

export default function setupRoute(app: Express, client: Client) {

  const productRepository = new ProductRepository(client);
  const productController = new ProductController(productRepository);

  app.route('/products')
    .get(productController.getProducts.bind(productController))
  
  app.route('/products/:id')
    .get(productController.getProduct.bind(productController))  
} 