// @flow
import Express from 'express';
import {
  PromoRepository
} from '../core/repositories';
import {
  PromoController
} from '../controllers';

export default function setupRoute(app: Express, client: Client) {

  const promoRepository = new PromoRepository(client);
  const promoController = new PromoController(promoRepository);

  app.route('/promos')
    .get(promoController.getPromos.bind(promoController))
    .post(promoController.insertPromo.bind(promoController))
    .put(promoController.updatePromo.bind(promoController))

  app.route('/promos/:id')
    .get(promoController.getPromo.bind(promoController))
} 